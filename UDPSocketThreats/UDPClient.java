package UDPSocket;
import java.io.*;
import java.net.*;
 
/**
 * This program demonstrates a UDP client
 * @author jl922223
 * @version 1.0
 * @since 2020-12-12
 */
public class UDPClient {
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// set the client address (IP) and send/receive ports
		InetAddress serverIP = InetAddress.getLocalHost(); // local IP address
		int sendPort = 7077;
		int recvPort = 7076;
		
		// Set the client address (IP) and send/receive ports from user input (in case you export the program as JAR)
		// add your code here
		
		new UDPClient().start(serverIP, sendPort, recvPort);
	}
 
	/**
	 * Starts the UDP client with 2 threads (Send Thread and Receive Thread)
	 * @param serverIP
	 * @param sendPort
	 * @param recvPort
	 * @throws SocketException
	 */
	private void start(InetAddress serverIP, int sendPort, int recvPort) throws SocketException {
		// Initiating the Client Send Thread
		// add your code here
		
		// Initiating the Client Receive Thread
		// add your code here
	}
 	
	/**
	 * ClientSendThread class for client sending messages
	 */
	class ClientSendThread implements Runnable {
	    private InetAddress serverAddress;
	    private int sendPort;
	    
		/**
		 * ClientSendThread constructor
		 * 
		 * @param serverIP
		 * @param sendPort
		 * @throws SocketException
		 */
		public ClientSendThread(InetAddress serverIP, int sendPort) throws SocketException {
	        serverAddress = serverIP;
	        this.sendPort = sendPort;
		}

		
		/**
		 * When an object implementing interface Runnable is used to create a thread,
		 * starting the thread causes the object's run method to be called in that separately executing thread. 
		 */
		@Override
		public void run() {
			try {
				sendMsg();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		/**
		 * sendMsg method send messages to server socket
		 * @throws IOException
		 */
		void sendMsg() throws IOException {
			System.out.println("-- Running UDP Client at " + InetAddress.getLocalHost() + " --");
			System.out.println("Enter message >");
			
			// Initiating the Datagram Socket 
			// add your code here

			// Listen to user input and encapsulated in a Datagram Packet
			do{
				// Take user input using e.g, BufferedReader
				// add your code here
				

				/* Encapsulate user input in a Datagram Packet using DatagramPacket
				 * for DatagramPacket you need Parameters:
				 * send packet. 
				 * data.length = packet length.
				 * address =e destination address.
				 */
	            // add your code here
				
	            // Send the packet using DatagramSocket Initiated object
				// add your code here
				
			} while(!input.equals("exit"));
			
			// Close the DatagramSocket
			// add your code here
		}
	}
	
	
	/**
	 * ClientRecvThread class for client messages receiving
	 */
	class ClientRecvThread implements Runnable {
	    private int recvPort;
	    
		/**
		 * ClientRecvThread constructor
		 * @param recvPort
		 */
		public ClientRecvThread(int recvPort) {
	        this.recvPort = recvPort;
		}

		/**
		 * When an object implementing interface Runnable is used to create a thread,
		 * starting the thread causes the object's run method to be called in that separately executing thread. 
		 */
		@Override
		public void run() {
			try {
				recvMsg();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		/**
		 * recvMsg method listen to the client socket for messages sent from server
		 * @throws IOException
		 */
		void recvMsg() throws IOException { 
			// add your code to complete this method
			// Listen to the client receive port for new messages from server 
			
			// Initiating the Datagram Socket on receive port
			
			/* Initiating DatagramPacket - Parameters are:
			 * 1-buffer for holding the incoming datagram.
			 * 2-length the number of bytes to read.
			 */
			
			// Fetch the packet data using DatagramSocket object receive method
			
			// Print the message contents

			// Close the DatagramSocket			
		}
	}
}