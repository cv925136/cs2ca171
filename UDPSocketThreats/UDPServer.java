package UDPSocket;
import java.io.*;
import java.net.*;
 
/**
 * This program demonstrates a UDP server
 * @author jl922223
 * @version 1.0
 * @since 2020-12-12
 */
public class UDPServer {
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// set the client address (IP) and send/receive ports
		InetAddress serverIP = InetAddress.getLocalHost(); // local IP address
		int sendPort = 7076;
		int recvPort = 7077;
			
		// Set the client address (IP) and send/receive ports from user input (in case you export the program as JAR)
		// add your code here
		
		new UDPServer().start(serverIP, sendPort, recvPort);
	}
 
	/**
	 * Starts the UDP server with 2 threads (Send Thread and Receive Thread)
	 * @param serverIP
	 * @param sendPort
	 * @param recvPort
	 * @throws SocketException
	 */
	private void start(InetAddress serverIP, int sendPort, int recvPort) throws SocketException {
		// Initiating the server Send Thread
		// add your code here	
		
		// Initiating the server Receive Thread
		// add your code here	
	}
	
	/**
	 * ServerSendThread class for server sending messages
	 */
	class ServerSendThread implements Runnable {
	    private InetAddress serverAddress;
	    private int sendPort;
	    
		/**
		 * ServerSendThread constructor
		 * 
		 * @param serverIP
		 * @param sendPort
		 * @throws SocketException
		 */
		public ServerSendThread(InetAddress serverIP, int sendPort){
	        serverAddress = serverIP;
	        this.sendPort = sendPort;
		}
		
		/**
		 * When an object implementing interface Runnable is used to create a thread,
		 * starting the thread causes the object's run method to be called in that separately executing thread. 
		 */		
		@Override
		public void run() {
			try {
				sendMsg();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		/**
		 * sendMsg method send messages to client socket
		 * @throws IOException
		 */		
		void sendMsg() throws IOException {
			System.out.println("-- Running UDP Server at " + InetAddress.getLocalHost() + " --");
			System.out.println("Enter message >");
			
			// Initiating the Datagram Socket 
			// add your code here	
			
			// Listen to user input and encapsulated in a Datagram Packet
			do{
				// Take user input using e.g, BufferedReader
				// add your code here	
				
				/* Encapsulate user input in a Datagram Packet using DatagramPacket
				 * for DatagramPacket you need Parameters:
				 * send packet. 
				 * data.length = packet length.
				 * address =e destination address.
				 */
				// add your code here	

				
	            // Send the packet using DatagramSocket Initiated object
				// add your code here	
				
			} while(!input.equals("exit"));
			// Close the DatagramSocket
			// add your code here	
		}

		/**
		 * sendMsgBack method can be called to respond to a client message
		 * @param msgToUpperCase
		 * @throws IOException
		 */
		public void sendMsgBack(String msgToUpperCase) throws IOException {
			//Initiating DatagramSocket and DatagramPacket similar to sendMsg method
			// add your code here
		}
	}
 
	
	/**
	 * ServerRecvThread class for server messages receiving
	 */
	class ServerRecvThread implements Runnable {
	    private int recvPort;
	    
	    
		/**
		 * ServerRecvThread constructor
		 * @param recvPort
		 */ 
		public ServerRecvThread(int recvPort) {
	        this.recvPort = recvPort;
		}

		/**
		 * When an object implementing interface Runnable is used to create a thread,
		 * starting the thread causes the object's run method to be called in that separately executing thread. 
		 */
		@Override
		public void run() {
			try {
				recvMsg();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		/**
		 * recvMsg method listen to the server socket for messages sent from client
		 * @throws IOException
		 */
		void recvMsg() throws IOException {
			// add your code to complete this method
			// Listen to the server receive port for new messages from client 

			// Initiating the Datagram Socket on receive port

			/* Initiating DatagramPacket - Parameters are:
			 * 1-buffer for holding the incoming datagram.
			 * 2-length the number of bytes to read.
			 */
			
			// Fetch the packet data using DatagramSocket object receive method
			
			// Print the message contents
	
			// Close the DatagramSocket

			
			// Send response to client using ServerSendThread class and sendMsgBack message

			}
		}
	}
}