package WWWServer;

/**
 * This program fetches an HTTP request - You need to complete the code for processRequest method
 * Complete when it say "add your code here"
 * @author jl922223
 */

final class HttpRequest implements Runnable {
	final static String CRLF = "\r\n";
	Socket socket;

	// Constructor
	public HttpRequest(Socket socket) throws Exception {
		this.socket = socket;
	}

	// The run() method of the Runnable interface.
	public void run() {
		try {
			processRequest();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	//  You need to complete the code for this method to parse an HTTP request
	private void processRequest() throws Exception {
		// Get a reference to the socket's input stream.
		// add your code here

		// Get a reference to the socket's output stream.
		// add your code here
	
		// Set up input stream filters using BufferedReader.
		// add your code here

		// Get the request line of the HTTP request message.
		// add your code here

		// Extract the filename from the requestLine using StringTokenizer.
		StringTokenizer tokens = new StringTokenizer(requestLine);
		
		// Try to run to check what the tokens object contain 
		//System.out.println(tokens.countTokens());
		//System.out.println(tokens.nextToken());
		//System.out.println(tokens.nextToken());
		// Note tokens.nextToken(); skip over the method, which should be "GET"
		
		// Now you need to extract the requested file-name as string from the the tokens object
		// add your code here


		// Append a "public-html" to the file-name so that file request is within the web file directory which is public-html.
		// add your code here
		
		// Optional set index.html as default page when fileName is empty

		// Open the requested file using FileInputStream.
		FileInputStream fis = null;
		boolean fileExists = true;
		try {
			fis = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			fileExists = false;
		}

		// Debug info printed in the console for private use
		System.out.println("Incoming!!!");
		System.out.println(requestLine);
		System.out.println(fileName);
		String headerLine = null;
		while ((headerLine = br.readLine()).length() != 0) {
			System.out.println(headerLine);
		}	

		// construct the response message.
		String statusLine = null; // use for status e.g. "HTTP/1.0 200 OK" or   HTTP/1.0 404 Not Found"
		String contentTypeLine = null; // use for content type e.g. "Content-Type: text/html"
		String entityBody = null; // use for entityBody in case file not found 
		
		// Set the statusLine, contentTypeLine (using the contentType method) and entityBody 
		// If file exists=true, then statusLine = "HTTP/1.0 200 OK" + CRLF; and contentTypeLine = "Content-Type: " + contentType(fileName) + CRLF;
		// If file exists=false, then statusLine = "HTTP/1.0 404 Not Found" + CRLF; and you need to set the respond entityBody.

		
		// Send the status line using output stream -> writeBytes(status line).
		// add your code here

		// Send the content type line using output stream -> writeBytes(content Type Line).
		// add your code here

		// Send a blank line to indicate the end of the header lines using output stream.
		// add your code here
		os.writeBytes(CRLF);

		// If fileExists - Send the entity body using the sendBytes method. If not fileExists - use output stream to writeBytes(entityBody);
		// add your code here

		
		// Close streams and socket.
		fis.close();
		is.close();
		os.close();
		br.close();
		socket.close();
	}

	private static void sendBytes(FileInputStream fis, OutputStream os) throws Exception {
		// Construct a 1K buffer to hold bytes on their way to the socket.
		byte[] buffer = new byte[1024];
		int bytes = 0;

		// Copy requested file into the socket's output stream.
		while ((bytes = fis.read(buffer)) != -1) {
			os.write(buffer, 0, bytes);
		}
	}

	private static String contentType(String fileName) {
		// Check if the file name end with ".html" or ".htm"
		if (fileName.endsWith(".htm") || fileName.endsWith(".html")) {
			return "text/html";
		}
		return "application/octet-stream";
	}
}