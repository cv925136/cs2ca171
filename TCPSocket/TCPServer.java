package TCPSocket;
import java.io.BufferedReader;
/**
 * This program demonstrates a TCP server 
 * @author jl922223
 * @version 1.0
 * @since 2020-12-12
 */
public class TCPServer{
    private ServerSocket server;
    /**
     * The TCPServer constructor initiate the socket
     * @param ipAddress
     * @param port
     * @throws Exception
     */
    public TCPServer(String ipAddress, int port) throws Exception {
		//add your code here
    }
    
    /**
     * The listen method listen to incoming client's datagrams and requests
     * @throws Exception
     */
    private void listen() throws Exception {
    	// listen to incoming client's requests via the ServerSocket
    	//add your code here

        // print received datagrams from client
        //add your code here
        }
    }
    
    public InetAddress getSocketAddress() {
        return this.server.getInetAddress();
    }
    
    public int getPort() {
        return this.server.getLocalPort();
    }
      
    public static void main(String[] args) throws Exception {
    	// set the server address (IP) and port number
    	String serverIP = "192.168.56.1"; // local IP address
    	int port = 7077;
    	
		if (args.length > 0) {
			serverIP = args[0];
			port = Integer.parseInt(args[1]);
		}
		// call the constructor and pass the IP and port
		//add your code here
    }
}