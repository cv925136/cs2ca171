package SMTP;


import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * 
 * Represents an SMTP Server application. 
 * Stores all information required to define the Server and handle Client connections.
 *  * 
 * @author JamesWalford
 *
 */
public class Server {
	
    /**
     * Server Application main method.
     * The main method accepts an ip address and port number as arguments
     * If arguments are provided it overrides the base values.
     * These values are used to instantiate a ServerSocket object.
     * The ServerSocket is used to handle client connections, creating new respective sockets.
     * Also required user input is requested, formatted and stored.
     * 
     * @param args The command line arguments.
     * @throws Exception
     **/
    public static void main(String[] args) throws Exception {

    	String serverIP = "192.168.56.1";  // Base local IP address
    	int port = 25; // Base Port Number
    
		if (args.length > 0) {	// If arguments provided, override the base values	
			serverIP = args[0]; 
			port = Integer.parseInt(args[1]);
		}
		
    	Scanner initialScanner = new Scanner(System.in); // Scan System.in for user input 
		System.out.println("\nEnter Server Name: "); // Request server name
		String hostName = initialScanner.nextLine(); // Read and store the server name
		initialScanner.close(); // Close the scanner
		
    	ServerSocket server; 
    	
		if (serverIP != null && !serverIP.isEmpty()) // Instantiate a ServerSocket (with base values or provided arguments) 
	          server = new ServerSocket(port, 1, InetAddress.getByName(serverIP));
	    else
	          server = new ServerSocket(0, 1, InetAddress.getLocalHost());
		
        System.out.println("\r\nRunning Server: " +  "Host=" + server.getInetAddress().getHostAddress() + " Port=" + port); // Output that the server is running
                
		while(true) {
			Socket connection = server.accept(); // Accept any client connection requests, creating a new respective socket
			processingSMTP SMTP = new processingSMTP(connection, hostName); // Instantiate a processingSMTP object to handle the connection
			Thread thread = new Thread(SMTP); // Instantiate a new thread to handle execution of the new connection.
			thread.start(); // Start thread execution
		}
	}
	
}

