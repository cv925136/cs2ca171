package SMTP;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * 
 * An object execute using a separate thread to handle a specific client, server connection. 
 * Stores all information required to handle SMTP messages received from the client.
 * Comprised of 2 main methods for sending and listening to TCP messages through sockets.
 * 
 * @author JamesWalford
 *
 */
public class processingSMTP implements Runnable{

	private Scanner scanner; 		// Used to scan System.in for user input
	private Socket client;			// Socket used to send and receive TCP messages
	private String senderEmail; 	// Stores sender email address
	private String recieverEmail;	// Stores recipient email address
	private String hostName;		// Stores server host name
	private boolean recievingData;	// Boolean indicate whether the server is expecting mail content
	private File file;				// Stores file written to
	private FileWriter fw;			//  Writers are used to format and write to a specified file
	private BufferedWriter bw;
	private PrintWriter pw;
	
	/**
	 * processingSMTP Constructor: Initialises an object used to handle client requests.
	 * Stores
	 * 	-> The socket connected to the client
	 *  -> The server domain name
	 * 
	 * @param serverAddress Server IP Address
	 * @param serverPort ServerSocket port #
	*/
    public processingSMTP(Socket client, String hostName) throws Exception {
    	this.client = client;
		this.hostName = hostName;
    }
    
	/**
	 * This method is called when the thread starts its execution
	*/
    public void run() {
		try {
			listen(); // Call the listen method (Listens for and responds to TCP messages)
		} catch (Exception e) {
			System.out.println(e);
		}
	}
    
    private void listen() throws Exception {

    	recievingData = false; // Initial value for starting SMTP stage
        String data = null; // No messages to store
        
        String clientAddress = client.getInetAddress().getHostAddress(); // Obtains the client address
 
        send(client, "220 " + hostName); // Sends message indicating that the SMTP service is ready
        System.out.println("\r\nNew client connection from " + clientAddress); // Output details of the new connection
       
        BufferedReader in = new BufferedReader (new InputStreamReader(client.getInputStream(), "US-ASCII")); // Read and decode received Telnet messages
        
        while ( (data = (in.readLine().toString())) != null ) { // While receiving messages
            System.out.println("\r\nMessage from " + clientAddress + ": " + data); // Output received messages
       
            StringTokenizer tokens = new StringTokenizer(data); // Tokenize string
        	String tokenValue = tokens.nextToken(); // Obtain first part of the SMTP message
        	
        	if (!recievingData) { // While not expecting mail content
        		
        		switch (tokenValue) {
        		
        			case "HELLO":
        				send(client, "250" +  "  Hello " + (tokens.nextToken().replace("<", "")).replace(">", "") + ", pleased to meet you"); // Send final application-level handshaking message
        				break;
        			case "MAIL":
                		tokens.nextToken();
                		senderEmail = (tokens.nextToken().replace("<", "")).replace(">", ""); // Store senders email address
                		send(client, "250 ok"); // Message client that the action has been taken and completed
        				break;
        			case "RCPT":
        				tokens.nextToken();
                		recieverEmail = (tokens.nextToken().replace("<", "")).replace(">", ""); // Store recipient email address
                		send(client, "250 ok"); // Message client that the action has been taken and completed
                		break;
        			case "DATA":
        				try {
                    		fw = new FileWriter(recieverEmail + ".txt", true); // Create/Open file representing recipient inbox
                    		bw = new BufferedWriter(fw);
                    		pw = new PrintWriter(bw);
                    		pw.println("Sender: " + senderEmail);  // Write/append the sender's email address to the text file      	             
                        } catch (IOException e) {
                            e.printStackTrace();                    
                        }
                        
                        recievingData = true; // Expecting mail content (Multiple messages requires no immediate response)
                		send(client, "End data with <CR><LF>.<CR><LF>"); // Message client with required formatting
                		break;       
        			case "QUIT":
                		send(client, "221 " + hostName + "  closing conection"); // Message the client that the service is closing
                		client.close();  // Close connection
                		break;			
        		}	
        	} else if (recievingData) { // Expecting mail content
        		
        		switch (tokenValue) {
        			
        			case ".": // Message finished (All text recieved)
        				send(client, "250 ok Message accepted for delivery"); // Message client that the action has been taken and completed       
                		pw.println("\n");
                		recievingData = false; // No longer expecting mail content
                		pw.close();  // Close all underlying writers/input streams
                		break;
        			default:
        				if (!data.contentEquals("DATA")){ // Mail content received
                			pw.println(data); // Write text to file
                		} 
        				break;			
        		}
        	} else {		
        		send(client, "Unrecognised Message"); // Catches unrecognised Telnet commands		
        	}
        }       
    }
    
	/**
	 * This method accepts a string argument and a socket.
	 * The method starts by getting the output stream of the socket.
	 * Then it encodes the string message as bytes using the US-ASCII character set.
	 * This newly encoded message is then written to the output stream and sent to the client.
	 * 
	 * @param client Socket connected to the client application
	 * @param msg String message to send
	*/
    private void send(Socket client, String msg) throws Exception {
    	OutputStream out = client.getOutputStream();
    	out.write((msg + "\r\n").getBytes("US-ASCII"));
    	
    }
}
